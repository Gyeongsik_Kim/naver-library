package com.gyungdal.naverlib;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Created by GyungDal on 2016-06-27.
 */
public class NaverLogin {
    private static final String NAVER_LOGIN = "https://nid.naver.com/nidlogin.login";
    private static final int ERROR_MAX = 5;

    private Context context;

    public NaverLogin(Context context){
        this.context = context;
    }

    public void setup(){
        WebView webView = new WebView(context);
        webView.setWebViewClient();
        CookieSyncManager.createInstance(context);
        CookieSyncManager.getInstance().startSync();
        CookieManager.getInstance().setAcceptCookie(true);
    }


    protected class ViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            view.setClickable(false);
            Log.d("Browse", url);
            if(url.contains("nidlogin.login")){
                Log.i(TAG, "stack : " + ++stack);
                if(stack >= ERROR_MAX){
                    dialog.dismiss();
                    CookieSyncManager.getInstance().stopSync();
                    web.setWebViewClient(null);
                    Toast.makeText(getApplicationContext(), "로그인 실패", Toast.LENGTH_SHORT).show();
                    process_login.this.startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    process_login.this.finish();
                }
            }
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (!url.contains("cafe")) {
                CookieSyncManager.getInstance().sync();
            } else if(url.equals("http://m.cafe.naver.com/onlyonedsm.cafe")) {
                CookieSyncManager.getInstance().sync();
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            Log.e(TAG, "ERROR CODE : " + errorCode);
            Log.e(TAG, description);
        }

        @Override
        public void onPageFinished(WebView v, String url){
            String Script = "var id = document.getElementById(\"id\");id.value = \"" + ID + "\";" +
                    "var pw = document.getElementById(\"pw\");pw.value = \"" + PASS + "\";" +
                    "document.getElementById(\"login_chk\").click();" +
                    "var button = document.getElementsByClassName(\"int_jogin\");" +
                    "for (var i=0;i<button.length; i++) {\n" +
                    "    button[i].click();\n" +
                    "};";
            v.evaluateJavascript(Script, null);
        }
    }
}
